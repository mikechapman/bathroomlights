int readit;

long milly;
long lastsec;
long lastmotion;
long lastpoweroff;

int avgread;
int numreads;
int timeremaining;
int highesttimer;

int firstrun;

void setup() {
  timeremaining = 0;
  lastpoweroff = 1;
  highesttimer = 0;
  milly = 0;
  lastsec = 0;
  readit = 0;
  avgread = 0;
  numreads = 1;
  firstrun = 10;

  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(4, OUTPUT);
  digitalWrite(4, LOW);

}

void relayops() {
  int fakeread;
  if (firstrun == 0)
  {
    if (timeremaining > 1)
    {
      digitalWrite(4, HIGH);
    }
    else
    {
      milly = millis() / 1000;
      lastpoweroff = milly;
      digitalWrite(4, LOW);
    }
  }
  else
  {
    /* Artificial bump */
    fakeread = analogRead(A0);
    avgread = 0 + fakeread;
    numreads = 4;
    timeremaining = 0;
  }
}

void detectedmotion()
{
  Serial.print(" Motion detected ");
  milly = millis() / 1000;
  if (timeremaining < 1)
  {
    if ((milly - lastpoweroff) > 30)
    {
      highesttimer = 90;
    }
    else
    {
      timeremaining = highesttimer;
      digitalWrite(4, HIGH);
    }
  }
  if ((milly - lastmotion) > 2)
  {
    if (timeremaining > 1)
    {
      if (timeremaining < 900)
      {
        timeremaining += 45;
        if (timeremaining > 360)
        {
          timeremaining += 60;
        }
      }
    }
    else
    {
      timeremaining = 90;
    }
    if (timeremaining > 1)
    {
      if (timeremaining > highesttimer)
      {
        highesttimer = timeremaining;
      }
    }
    lastmotion = milly;
    Serial.println(" PROCESSED due to NO recent motion");
  }
  else
  {
    Serial.println(" IGNORED due to recent motion");
  }


}

void everytwo()
{
  int overallavg;

  overallavg = avgread / numreads;
  Serial.print("AnalogRead() = ");
  Serial.print(readit);
  Serial.print(" / ");
  Serial.print(overallavg);
  Serial.print(" (");
  Serial.print(numreads);
  Serial.println(")   ");

  if ((overallavg > 1) && (firstrun < 1))
  {
    detectedmotion();
  }

  avgread = 0;
  numreads = 1;

}
void loop() {

  milly = millis() / 1000;

  if ((milly - lastsec) > 1)
  {
    everytwo();
    lastsec = milly;
    relayops();
    timeremaining--;
    relayops();
    if (firstrun > 0)
    {
      firstrun--;
      Serial.print("FIRSTRUN = ");
      Serial.println(firstrun);
      lastmotion = milly;
    }
    else
    {
      if (timeremaining > 0)
      {
        Serial.print(" Time remaining = ");
        Serial.println(timeremaining);
      }
    }
  }
  else
  {
    delay(15);
    if (numreads < 512)
    {
      numreads++;
      readit = analogRead(A0);
      if (readit > 5)
      {
        delay(25);
        readit = analogRead(A0);
        if (readit > 8)
        {
          delay(35);
          readit = analogRead(A0);
          if (readit > 10)
          {
            avgread += 30;
            if (readit > 15)
            {
              delay(20);
              readit = analogRead(A0);
              if (readit > 10)
              {
                delay(5);
                readit = analogRead(A0);
                if (readit > 10)
                {
                  avgread = 19999;
                  numreads = 510;
                }

              }
            }
          }
        }
      }
      else
      {
        avgread += 0;
      }
    }
  }
}
